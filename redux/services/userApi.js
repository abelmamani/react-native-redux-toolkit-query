import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
export const userApi = createApi({
    reducerPath: 'userApi',
    baseQuery: fetchBaseQuery({ baseUrl: 'https://rickandmortyapi.com/api' }),
    endpoints: (builder) => ({
      getUsers: builder.query({
          query: () => ({
              url: '/character',
              method: 'Get',
          }),
      }),
    }),
  });

  export const {useGetUsersQuery} = userApi;