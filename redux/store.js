import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/dist/query';
import { userApi } from './services/userApi';

export const store = configureStore({
  reducer: {
    userApi: userApi.reducer,
    // Add your other reducers here
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat([userApi.middleware])

});
setupListeners(store.dispatch);