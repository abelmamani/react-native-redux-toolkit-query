# aplicacion mobile con React Native + Expo
* Uso de redux toolkit query para menejar el estado de apis. 
* Uso de tailwincss para estilos.
* uso de react-native navigation

# comandos mas utlizados 
* npm install -g expo-cli
* npx create-expo-app react-native-crud
* npm i @reduxjs/toolkit
* npm i react-redux     
* npm i tailwind-react-native-classnames
* npm install react-native-elements
* npm install react-native-vector-icons
* npm i react-native-safe-area-context
* npm i @react-navigation/native
* npm i @react-navigation/stack 
* npm i react-native-dotenv
* npm i @react-navigation/drawer
* npx expo install react-native-screens react-native-safe-area-context
* npx expo install react-native-web@~0.19.6 react-dom@18.2.0 @expo/webpack-config@^19.0.0
* npm install @react-navigation/native @react-navigation/bottom-tabs
