import { NavigationContainer } from '@react-navigation/native';
import { StyleSheet } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Nav } from './components/Nav';
import { Provider } from 'react-redux';
import {store} from './redux/store';
export default function App() {
  return (
    <Provider store={store} >
    <NavigationContainer>
      <SafeAreaProvider>
        <Nav></Nav>
      </SafeAreaProvider>
    </NavigationContainer>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
