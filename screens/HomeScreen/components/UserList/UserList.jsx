import { useNavigation } from '@react-navigation/native'; // Importa useNavigation
import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useGetUsersQuery } from '../../../../redux/services/userApi';
import tw from "tailwind-react-native-classnames";
export const UserList = () => {
    const { data: users, isLoading, isError } = useGetUsersQuery();
    const navigation = useNavigation();

    if (isLoading) {
      return <Text>Cargando usuarios...</Text>;
    }
  
    if (isError) {
      return <Text>Error al cargar usuarios.</Text>;
    }

    const handleUserPress = (user) => {
      navigation.navigate('AboutScreen', { user });
    };
  
    return (
      <View>
        <Text style = {tw`text-center text-xl font-bold font-semibold`}>Personajes</Text>
        <FlatList
          data={users.results}
          keyExtractor={(user) => user.id.toString()}
          style = {tw`p-3`}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => handleUserPress(item)}>
            <View style={tw`flex flex-row  items-center p-2`}>
            <Image source={{ uri: item.image }} style={tw`w-12 h-12 rounded-3xl mr-2`}/>
            <Text style = {tw`text-center text-lg font-semibold mr-2`}>{item.name}</Text>
          </View>
          </TouchableOpacity>
          )}
        />
      </View>
      
    );
};
