import { SafeAreaView } from 'react-native-safe-area-context';
import { UserList } from './components/UserList/UserList';
export const HomeScreen = () => {
    return (
        <SafeAreaView>
            <UserList></UserList> 
        </SafeAreaView>
   
    )
}
