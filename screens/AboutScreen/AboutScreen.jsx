import { Image, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import tw from "tailwind-react-native-classnames";
export const AboutScreen = ({route}) => {
    const { user } = route.params; 
    return (
        <SafeAreaView style={tw`flex flex-col items-center p-2`}>
            <Text style = {tw` text-center mt-2 text-xl font-semibold mb-5`}>Personaje {user.id}</Text>
            <Image source={{uri: user.image}} style = {[tw`rounded-3xl`, {width: 120, height: 120, resizeMode: "contain"}]}/>
            <Text style = {tw` text-center mt-2 text-lg font-semibold`}>{user.name}</Text>
            <Text style = {tw`bg-black rounded-xl text-white p-1 mt-2 text-center text-sm font-semibold`}>{user.status}</Text>
            <TouchableOpacity style={tw`bg-blue-300 flex-row items-center px-8 py-3 rounded-2xl mt-2`} onPress={() => navigation.navigate('HomeScreen')}><Text tyle = {tw`text-center py-5 text-xl font-semibold`}>Volver</Text></TouchableOpacity>
        </SafeAreaView>
       
    );
};
