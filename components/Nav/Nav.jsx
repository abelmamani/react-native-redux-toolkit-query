import { createStackNavigator } from '@react-navigation/stack';
import { HomeScreen } from '../../screens/HomeScreen/HomeScreen';
import {AboutScreen} from '../../screens/AboutScreen/AboutScreen';
const Stack = createStackNavigator();
const Nav = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name = 'HomeScreen' component={HomeScreen} options = {{headerShown: false}}/>
            <Stack.Screen name = 'AboutScreen' component={AboutScreen} options = {{headerShown: false}}/>
        </Stack.Navigator>
    )
}

export default Nav;