import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { HomeScreen } from '../../screens/HomeScreen/HomeScreen';
import {AboutScreen} from '../../screens/AboutScreen/AboutScreen';
const Tab = createBottomTabNavigator();
const Nav = () => {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Home" component={HomeScreen} />
            <Tab.Screen name="About" component={AboutScreen} />
        </Tab.Navigator>
    )
}

export default Nav